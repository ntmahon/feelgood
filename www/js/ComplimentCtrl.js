(function () {
    'use strict';

    angular.module('feelgood').controller('ComplimentCtrl', ['$scope', '$ionicPopup', ComplimentCtrl]);

    function ComplimentCtrl($scope, $ionicPopup) {

   $scope.compliment = function() {
    var arr = ["You are good enough", 
            "You are making a difference", 
            "You look great in that outfit", 
            "You are so charming", 
            "What a nice smile", 
            "Go on, you've earned it", 
            "I would give you my last piece of chocolate", 
            "You're perfect the way you are", 
            "You're doing a great job"];

    var compliment = arr[Math.floor(Math.random() * arr.length)];

     var alertPopup = $ionicPopup.alert({
       title: ':)',
       template: compliment,
       buttons: [ {text: "Back" } ]
     });
    };
};
})();