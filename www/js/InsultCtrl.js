(function () {
    'use strict';

    angular.module('feelgood').controller('InsultCtrl', ['$scope', '$ionicPopup', InsultCtrl]);


    function InsultCtrl($scope, $ionicPopup) {

   $scope.insult = function() {
    var arr = ["Seriously, why did you bother?", 
            "At least you tried... I guess", 
            "Those shoes with that shirt?",
            "I feel nauseous in your presence",
            "Is your mother proud?"];

    var insult = arr[Math.floor(Math.random() * arr.length)];

     var alertPopup = $ionicPopup.alert({
       title: ':(',
       template: insult,
       buttons: [ {text: "Back" } ]
     });
    };
};
})();